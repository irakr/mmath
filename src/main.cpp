#include <iostream>
#include <cstring>
#include "mmath.h"

int main(void)
{
    MMath* mmath = MMath::instance();
    std::cout << "Sum(1000, 2540) = " << mmath->sum(1000, 2540) << std::endl;
    return 0;
}