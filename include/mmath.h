#pragma once

#include <cmath>

class MMath
{
public:
    static inline MMath* instance()
    {
        if(!m_Instance) {
            m_Instance = new MMath;
        }
        return m_Instance;
    }

    double sum(double a, double b)
    {
        return a + b;
    }
    
private:
    MMath()
    {}
    static MMath* m_Instance;
};